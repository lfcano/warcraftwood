package com.warcraft.wood.center

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.Gson
import com.warcraft.wood.data.api.ApiMovies
import com.warcraft.wood.data.db.MovieCartDao
import com.warcraft.wood.data.db.MovieDataBase
import com.warcraft.wood.data.db.MoviesDao
import com.warcraft.wood.util.Util
import okhttp3.OkHttpClient

abstract class BaseFragment : Fragment() {

    protected lateinit var okHttpClient: OkHttpClient
    protected lateinit var retrofit: ApiMovies
    protected lateinit var gosn: Gson
    protected lateinit var movieDao: MoviesDao
    protected lateinit var movieCartDao: MovieCartDao
    protected lateinit var movieDataBase: MovieDataBase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(fragmentLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gosn = Util.gsonProvider()
        okHttpClient = Util.okHttpClientProvider()
        retrofit = Util.retrofitProvider(okHttpClient, gosn)
        movieDataBase = Util.dataBaseProvider(context!!)
        onFinishedViewLoad()
    }

    @LayoutRes
    abstract fun fragmentLayout(): Int

    abstract fun onFinishedViewLoad()

    protected fun navController(): NavController? {
        return view?.let { Navigation.findNavController(it) }
    }
}