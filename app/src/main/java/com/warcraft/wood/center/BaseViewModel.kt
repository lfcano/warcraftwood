package com.warcraft.wood.center

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.warcraft.wood.control.model.local.MoviesLocal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

open class BaseViewModel : ViewModel() {

    val liveData = MutableLiveData<List<MoviesLocal>>()
    val liveDataDetail = MutableLiveData<MoviesLocal>()
    private val disposables = CompositeDisposable()
    val liveDataCart = MutableLiveData<List<MoviesLocal>>()

    protected fun  addDisposable(observable: Observable<List<MoviesLocal>>){
        val disposables1 : Disposable = observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{
            }
            .subscribe ({
                liveData.postValue(it)
            },{
                Log.d("holi","holi2")
            })
        disposables.add(
            disposables1
        )
    }

    protected fun  addDisposableCart(observable: Observable<List<MoviesLocal>>){
        val disposables1 : Disposable = observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{
            }
            .subscribe ({
                liveDataCart.postValue(it)
            },{
                Log.d("holi","holi2")
            })
        disposables.add(
            disposables1
        )
    }

    protected fun  addDisposableDetail(observable: Observable<MoviesLocal>){
        disposables.add(observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{
            }
            .subscribe ({
                liveDataDetail.postValue(it)
                Log.d("addDisposableDetail", it.toString())
            },{
                Log.d("holiDetail","holiDetail")
            })
        )
    }

    override fun onCleared() {
        disposables.clear()
    }
}