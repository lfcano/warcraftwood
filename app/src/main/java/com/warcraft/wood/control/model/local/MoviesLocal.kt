package com.warcraft.wood.control.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "MoviesLocal")
data class MoviesLocal(
    @PrimaryKey
    val id: String,
    @SerializedName("title")
    val originalName: String,
    @SerializedName("poster_path")
    val imagesmovies: String,
    val overview: String

) {
    override fun toString(): String {
        return "MoviesLocal(id='$id', originalName='$originalName', imagesmovies='$imagesmovies', overview='$overview')"
    }
}