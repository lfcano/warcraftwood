package com.warcraft.wood.control.model.remote

import com.warcraft.wood.control.model.local.MoviesLocal
import com.google.gson.annotations.SerializedName

data class MoviesRemote(
    var page: Int,
    var results: List<MoviesLocal> = emptyList(),
    @SerializedName("total_results")
    var totalResults: Int,
    @SerializedName("total_pages")
    var totalPages: Int
)
