package com.warcraft.wood.control.repository

import android.util.Log
import com.google.gson.Gson
import com.warcraft.wood.control.model.local.MoviesLocal
import com.warcraft.wood.control.model.remote.MoviesRemote
import com.warcraft.wood.data.api.ApiMovies
import com.warcraft.wood.data.db.MovieCartDao
import com.warcraft.wood.data.db.MoviesDao
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MoviesRepository(
    private val apiMovies: ApiMovies,
    private val moviesDao: MoviesDao,
    private val movieCartDao : MovieCartDao
    ) {

    fun movies() : Observable<List<MoviesLocal>>{
        return getMoviesPopular().onErrorResumeNext(movieFromBd())
    }

    fun detaiMovieBd(movieId : String) : Observable<MoviesLocal>{
        return getDetailMovie(movieId).onErrorResumeNext(detailFromBd(movieId))
    }

    fun getMoviesPopular(): Observable<List<MoviesLocal>> {
        return apiMovies.getPopularMovies().map { response ->
            Log.d("getPopular", response.toString())
            Gson().fromJson(response, MoviesRemote::class.java).results
        }.doOnNext {
            saveMovies(it)
        }
    }

    fun getDetailMovie(movieId: String): Observable<MoviesLocal> {
        return apiMovies.getDetailMovie(movieId).map { response ->
            Gson().fromJson(response, MoviesLocal::class.java)
        }.doOnError {
                Log.e("getDetailMovie1", it.toString())
            }.doOnNext{
            saveMovie(it)
        }
    }

    fun saveMovies(moviesLocal: List<MoviesLocal>) {
        Observable.fromCallable { moviesDao.insertMovies(moviesLocal) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError {
                Log.e( "errorsave","Unable to store movie")
            }
            .doOnNext { Log.i("errorsave"," Movie stored!") }
            .subscribe()
    }


    fun saveMovie(moviesLocal: MoviesLocal) {
        Observable.fromCallable { moviesDao.insertMovie(moviesLocal) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    private fun movieFromBd() :  Observable<List<MoviesLocal>>{
        return moviesDao.movies().toObservable()
            .doOnNext{
                if (it.isNotEmpty()){
                    it
                }else{
                    Observable.just(emptyList<MoviesLocal>())
                }
            }
    }

    private fun detailFromBd(movieId: String): Observable<MoviesLocal> {
        return moviesDao.movie(movieId).filter { it != null }
            .toObservable()
            .doOnNext {
                Log.d("detailFromBd","Dispatching ${it.id} movie from DB...")
            }
    }

    fun addMovie(movie: MoviesLocal) {
        Observable.fromCallable { movieCartDao.addMovie(movie) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Log.e("addPrueba", "Unable to store movies in cart") }
            .doOnNext { Log.d("addPrueba","${movie.originalName} Movies in cart updated!") }
            .subscribe()
    }

    fun deleteMovie(movieId: String) {
        Observable.fromCallable { movieCartDao.deleteMovie(movieId) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Log.e(it.toString(), "Unable to store movies in cart") }
            .doOnNext { Log.i("prueba", "$movieId Movie deleted!") }
            .subscribe()
    }


    fun showMoviesCart(): Observable<List<MoviesLocal>> {
        return movieCartDao.all()
            .toObservable()
            .doOnError { Log.e(it.toString(), "Unable to get movies of cart") }
            .doOnNext {
                if(it.isNotEmpty()){
                    it
                }else{
                    Observable.just(emptyList<MoviesLocal>())
                }
            }
    }

    fun emptyCart() {
        Observable.fromCallable { movieCartDao.empty() }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnError { Log.e(it.toString(), "Unable to store movies in cart") }
            .doOnNext { Log.i("pruebaDop","Cart droped!") }
            .subscribe()
    }

}