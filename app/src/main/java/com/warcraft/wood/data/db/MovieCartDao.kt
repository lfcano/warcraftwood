package com.warcraft.wood.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.warcraft.wood.control.model.local.MoviesLocal
import io.reactivex.Single

@Dao
interface MovieCartDao {

    @Query("SELECT * FROM MoviesLocal")
    fun all(): Single<List<MoviesLocal>>

    @Query("SELECT * FROM MoviesLocal WHERE id = :movieId")
    fun searchMovie(movieId: String): Single<MoviesLocal>

    @Query("DELETE FROM MoviesLocal WHERE id = :movieId")
    fun deleteMovie(movieId: String): Int

    @Query("DELETE FROM MoviesLocal")
    fun empty(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMovies(movies: List<MoviesLocal>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMovie(movies: MoviesLocal)
}