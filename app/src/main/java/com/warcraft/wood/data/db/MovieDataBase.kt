package com.warcraft.wood.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.warcraft.wood.control.model.local.MoviesLocal

@Database(entities = [MoviesLocal::class], version = 1, exportSchema = false)
abstract class MovieDataBase : RoomDatabase(){
    abstract fun movieDao() : MoviesDao
    abstract fun movieCartDao() : MovieCartDao
}