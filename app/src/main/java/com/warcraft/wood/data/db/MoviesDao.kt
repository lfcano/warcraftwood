package com.warcraft.wood.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.warcraft.wood.control.model.local.MoviesLocal
import io.reactivex.Single


@Dao
interface MoviesDao {
    @Query("SELECT * FROM MoviesLocal WHERE id = :movieId")
    fun movie(movieId: String): Single<MoviesLocal>

    @Query("SELECT * FROM MoviesLocal WHERE id ")
    fun movies(): Single<List<MoviesLocal>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovies(movies: List<MoviesLocal>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movies: MoviesLocal)

}