package com.warcraft.wood.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.warcraft.wood.R
import com.warcraft.wood.control.model.local.MoviesLocal
import kotlinx.android.synthetic.main.adapter_popular_movies.view.*
import java.util.*

class CartAdapter (var cartData : List<MoviesLocal>) :
    RecyclerView.Adapter<CartAdapter.CartHolder>(){


    inner class CartHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
        val mImagenCart = itemView.imageMovies
        val mTitleCart = itemView.tvTitleMovies

        init {
        }
        override fun onClick(v: View?) {
            val position = adapterPosition
            val i = v!!.id
            if (i == R.id.imageMovies){
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_popular_movies, parent, false)
        return CartHolder(view)
    }

    override fun onBindViewHolder(holder: CartHolder, position: Int) {
        Picasso.get()
            .load(PATH + cartData[position].imagesmovies)
            .resize(400, 600)
            .centerCrop()
            .into(holder.mImagenCart)

        holder.mTitleCart.text = cartData[position].originalName
        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = cartData.size

    private var lastPosition = -1

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val anim = ScaleAnimation(
                0.0f,
                1.0f,
                0.0f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            anim.duration = Random().nextInt(701).toLong()//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim)
            lastPosition = position
        }
    }
}