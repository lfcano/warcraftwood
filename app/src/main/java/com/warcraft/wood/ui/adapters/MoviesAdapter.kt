package com.warcraft.wood.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.recyclerview.widget.RecyclerView
import com.warcraft.wood.ui.interfaces.DetailMoviesInterface
import com.squareup.picasso.Picasso
import com.warcraft.wood.R
import com.warcraft.wood.control.model.local.MoviesLocal
import kotlinx.android.synthetic.main.adapter_popular_movies.view.*
import java.util.*

const val PATH = "https://image.tmdb.org/t/p/w500"

class MoviesAdapter(var movieData: List<MoviesLocal>) :
    RecyclerView.Adapter<MoviesAdapter.MoviesHolder>() {

    var detailMovies: DetailMoviesInterface? = null

    inner class MoviesHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val mImagenMovies = itemView.imageMovies
        val mTitleMovies = itemView.tvTitleMovies
        val mDetailMovies = itemView.tvDetailMovies
        val mBtnAddCart = itemView.btnAddCart
        val mBtnDeleteCart = itemView.btnDeleteCart

        init {
            mTitleMovies.setOnClickListener(this)
            mDetailMovies.setOnClickListener(this)
            mImagenMovies.setOnClickListener(this)
            mBtnAddCart.setOnClickListener(this)
            mBtnDeleteCart.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            val i = v!!.id
            if (i == R.id.imageMovies) {
                detailMovies!!.onDetailMovies(movieData[position])
            } else if (i == R.id.tvTitleMovies) {
                detailMovies!!.onDetailMovies(movieData[position])
            } else if (i == R.id.tvDetailMovies) {
                detailMovies!!.onDetailMovies(movieData[position])
            } else if (i == R.id.btnAddCart){
                detailMovies!!.onAddCartMovie(movieData[position])
            }else if (i == R.id.btnDeleteCart){
                detailMovies!!.onDeteleAMovieCart(movieData[position])
            }else if( i == R.id.tvTitleMovies){
                detailMovies!!.onDetailMovies(movieData[position])
            }else if(i == R.id.tvDetailMovies){
                detailMovies!!.onDetailMovies(movieData[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_popular_movies, parent, false)
        return MoviesHolder(view)
    }

    override fun onBindViewHolder(holder: MoviesHolder, position: Int) {
        Picasso.get()
            .load(PATH + movieData[position].imagesmovies)
            .resize(400, 600)
            .centerCrop()
            .into(holder.mImagenMovies)

        holder.mTitleMovies.text = movieData[position].originalName
        holder.mDetailMovies.text = movieData[position].overview

        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = movieData.size

    fun onDetailsMovies(detailMoviesInterface: DetailMoviesInterface) {
        this.detailMovies = detailMoviesInterface
    }

    private var lastPosition = -1

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val anim = ScaleAnimation(
                0.0f,
                1.0f,
                0.0f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            anim.duration = Random().nextInt(701).toLong()//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim)
            lastPosition = position
        }
    }
}