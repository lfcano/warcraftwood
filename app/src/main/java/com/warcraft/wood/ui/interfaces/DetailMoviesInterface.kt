package com.warcraft.wood.ui.interfaces

import com.warcraft.wood.control.model.local.MoviesLocal


interface DetailMoviesInterface {
    fun onDetailMovies(moviesLocal: MoviesLocal)
    fun onAddCartMovie(movies: MoviesLocal)
    fun onDeteleAMovieCart(moviesLocal: MoviesLocal)
}