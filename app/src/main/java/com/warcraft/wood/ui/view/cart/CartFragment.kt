package com.warcraft.wood.ui.view.cart

import androidx.lifecycle.Observer
import com.warcraft.wood.R
import com.warcraft.wood.center.BaseFragment
import com.warcraft.wood.control.model.local.MoviesLocal
import com.warcraft.wood.control.repository.MoviesRepository
import com.warcraft.wood.data.db.MovieCartDao
import com.warcraft.wood.data.db.MovieDataBase
import com.warcraft.wood.data.db.MoviesDao
import com.warcraft.wood.ui.adapters.CartAdapter
import kotlinx.android.synthetic.main.fragment_cart.*

class CartFragment : BaseFragment() {

    lateinit var moviesRepository: MoviesRepository
    lateinit var moviesViewModel: CartViewModel
    private var adapterCart : CartAdapter? = null

    override fun onFinishedViewLoad() {
        initializeUi()

        btnEmptyCart.setOnClickListener {
            moviesViewModel.deleteMovies()
        }
    }


    private fun initializeUi() {
        movieDao =movieDao(movieDataBase)
        movieCartDao = movieCartDao(movieDataBase)
        moviesRepository = MoviesRepository(retrofit, movieDao, movieCartDao)
        moviesViewModel = CartViewModel(moviesRepository)
        moviesViewModel.showMovies()

        moviesViewModel.liveDataCart.observe(this, recyclerMovieCart)

    }

    private fun showMoviesCart(movies : List<MoviesLocal>){
        adapterCart = CartAdapter(movies)
        rvCartMovies.adapter = adapterCart
    }

    var recyclerMovieCart = Observer<List<MoviesLocal>> { moviesLocal ->
        showMoviesCart(moviesLocal)
    }
    override fun fragmentLayout(): Int = R.layout.fragment_cart

    fun movieDao(db: MovieDataBase): MoviesDao = db.movieDao()
    fun movieCartDao(db: MovieDataBase): MovieCartDao = db.movieCartDao()
}