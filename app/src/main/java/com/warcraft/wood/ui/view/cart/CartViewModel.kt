package com.warcraft.wood.ui.view.cart

import com.warcraft.wood.center.BaseViewModel
import com.warcraft.wood.control.repository.MoviesRepository

class CartViewModel(private val moviesRepository: MoviesRepository)
    : BaseViewModel(){

    fun showMovies(){
        addDisposableCart(moviesRepository.showMoviesCart())
    }

    fun deleteMovies(){
        moviesRepository.emptyCart()
    }
}