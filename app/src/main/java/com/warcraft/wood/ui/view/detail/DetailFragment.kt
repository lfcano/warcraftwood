package com.warcraft.wood.ui.view.detail

import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import com.warcraft.wood.R
import com.warcraft.wood.center.BaseFragment
import com.warcraft.wood.control.model.local.MoviesLocal
import com.warcraft.wood.control.repository.MoviesRepository
import com.warcraft.wood.data.db.MovieCartDao
import com.warcraft.wood.data.db.MovieDataBase
import com.warcraft.wood.data.db.MoviesDao
import kotlinx.android.synthetic.main.fragment_detail_movie.*

const val PATH_DETAIL_MOVIE = "https://image.tmdb.org/t/p/w500"
class DetailFragment : BaseFragment() {

    lateinit var moviesRepository: MoviesRepository
    lateinit var detailViewModel: DetailViewModel
    var movieId : String = ""

    override fun onFinishedViewLoad() {
        movieId = arguments!!.getString("movieId", movieId)
        initializeUi()

        btnCartDetail.setOnClickListener {
            navController()!!.navigate(DetailFragmentDirections.actionDetailFragmentToCartFragment())
        }
    }

    private fun initializeUi() {
        movieDao =movieDao(movieDataBase)
        movieCartDao = movieCartDao(movieDataBase)
        moviesRepository = MoviesRepository(retrofit, movieDao, movieCartDao)
        detailViewModel = DetailViewModel(moviesRepository)
        detailViewModel.getMovieDetails(movieId)
        detailViewModel.getDetailBd(movieId)

        detailViewModel.liveDataDetail.observe(this, viewDetails)

    }

    fun initViewDetailMovie(moviesLocal: MoviesLocal){
        titleDetail.text = moviesLocal.originalName
        overrideDetail.text = moviesLocal.overview
        Picasso.get()
            .load(PATH_DETAIL_MOVIE + moviesLocal.imagesmovies)
            .resize(600, 800)
            .centerCrop()
            .into(imageDetailMovie)
    }

    var viewDetails = Observer<MoviesLocal> { movieLocal ->
        initViewDetailMovie(movieLocal)
    }

    override fun fragmentLayout(): Int = R.layout.fragment_detail_movie

    fun movieDao(db: MovieDataBase): MoviesDao = db.movieDao()
    fun movieCartDao(db: MovieDataBase): MovieCartDao = db.movieCartDao()
}