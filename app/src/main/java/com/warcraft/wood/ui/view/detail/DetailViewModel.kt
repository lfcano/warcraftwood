package com.warcraft.wood.ui.view.detail

import com.warcraft.wood.center.BaseViewModel
import com.warcraft.wood.control.repository.MoviesRepository

class DetailViewModel (private val moviesRepository: MoviesRepository) : BaseViewModel() {

    fun getMovieDetails(movieId : String){
        addDisposableDetail(moviesRepository.getDetailMovie(movieId))
    }

    fun getDetailBd(movieId: String){
        addDisposableDetail(moviesRepository.detaiMovieBd(movieId))
    }
}