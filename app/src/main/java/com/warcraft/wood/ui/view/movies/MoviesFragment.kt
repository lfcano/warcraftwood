package com.warcraft.wood.ui.view.movies

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.warcraft.wood.R
import com.warcraft.wood.center.BaseFragment
import com.warcraft.wood.control.model.local.MoviesLocal
import com.warcraft.wood.control.repository.MoviesRepository
import com.warcraft.wood.data.db.MovieCartDao
import com.warcraft.wood.data.db.MovieDataBase
import com.warcraft.wood.data.db.MoviesDao
import com.warcraft.wood.ui.adapters.MoviesAdapter
import com.warcraft.wood.ui.interfaces.DetailMoviesInterface
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : BaseFragment() , DetailMoviesInterface{

    lateinit var moviesRepository: MoviesRepository
    lateinit var moviesViewModel: MoviesViewModel
    var mAdapter : MoviesAdapter? = null

    override fun onFinishedViewLoad() {
        initializeUi()

        btnCart.setOnClickListener {
            navController()!!.navigate(MoviesFragmentDirections.actionMovieFragmentToCartFragment())
        }
    }

    private fun initializeUi() {
        movieDao =movieDao(movieDataBase)
        movieCartDao = movieCartDao(movieDataBase)
        moviesRepository = MoviesRepository(retrofit, movieDao, movieCartDao)
        moviesViewModel = MoviesViewModel(moviesRepository)
        moviesViewModel.getMovies()
        moviesViewModel.getMovieBd()

        moviesViewModel.liveData.observe(this, recyclerPopular)
    }

    fun initAdapterMovies(moviesLocal: List<MoviesLocal>) {
        mAdapter = MoviesAdapter(moviesLocal)
        mAdapter!!.onDetailsMovies(this)
        rvMovies.adapter = mAdapter
    }

    var recyclerPopular = Observer<List<MoviesLocal>> { moviesLocal ->
        initAdapterMovies(moviesLocal)
    }

    override fun onAddCartMovie(movies: MoviesLocal) {
        moviesViewModel.addMovieCart(movies)
    }

    override fun onDeteleAMovieCart(moviesLocal: MoviesLocal) {
        moviesViewModel.deleteMovie(moviesLocal.id)
    }

    override fun onDetailMovies(moviesLocal: MoviesLocal) {
        var bundle = Bundle()
        bundle.putString("movieId", moviesLocal.id)
        Log.d("idprueba", moviesLocal.id)
        navController()!!.navigate(R.id.action_movieFragment_to_detailFragment, bundle)
    }

    override fun fragmentLayout(): Int = R.layout.fragment_movies

    fun movieDao(db: MovieDataBase): MoviesDao = db.movieDao()
    fun movieCartDao(db: MovieDataBase): MovieCartDao = db.movieCartDao()


}