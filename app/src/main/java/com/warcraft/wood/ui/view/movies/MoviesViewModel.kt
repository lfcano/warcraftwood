package com.warcraft.wood.ui.view.movies

import com.warcraft.wood.center.BaseViewModel
import com.warcraft.wood.control.model.local.MoviesLocal
import com.warcraft.wood.control.repository.MoviesRepository

class MoviesViewModel (private val moviesRepository: MoviesRepository)
    : BaseViewModel(){

    fun getMovies() {
        addDisposable(moviesRepository.getMoviesPopular())
    }

    fun getMovieBd(){
        addDisposable(moviesRepository.movies())
    }

    fun addMovieCart(movie: MoviesLocal){
        moviesRepository.addMovie(movie)
    }

    fun deleteMovie(movie : String){
        moviesRepository.deleteMovie(movie)
    }

    fun clearDisposable(){
        clearDisposable()
    }
}